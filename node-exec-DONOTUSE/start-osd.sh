#!/bin/bash
x=35
while [ $x -le 92 ]
do
  ssh 10.20.0.51 "initctl start ceph-osd id=$x"
  x=$(( $x + 3 ))
done
