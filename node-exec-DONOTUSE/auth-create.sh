#!/bin/bash
x=34
while [ $x -le 92 ]
do
  cd /var/lib/ceph/osd/ceph-$x
  ceph auth add osd.$x osd 'allow *' mon 'allow profile osd' -i ./keyring
  x=$(( $x + 3 ))
done
