## **Initializing New Ceph OSDs** ##

These scripts are designed for initializing and connecting Ceph OSDs from a Fuel Master.

Please be aware that these scripts should be executed PER SWEEP of OSD additions unless otherwise notated (i.e. adding one OSD per every host in the cluster during a single run).  These could be further modified for complete automation in most clusters, but that wasn't an option in the environment these were originally built on because there was an inconsistency in partitioning on (2) nodes, so the partition numbers didn't match.  Unfortunately, this means some manual intervention is required for inconsistent disks, but the general flow is as follows:

**DO THE FOLLOWING ONLY ONE TIME**

1. Generate an 'osd-hosts' file using some variation of 'fuel nodes | grep compute | awk {'print $9'}' (this step allows for manual removal of nodes from the process, which is sometimes necessary)
2. Modify and execute 'parted-loop' to generate the correct partitions required per disk (this script doesn't use nested looping for partition creation due to the inconsistency of disks in this environment, but could likely be used in other environments)

**DO THE FOLLOWING FOR EACH RUN OF OSD ADDITIONS**

1. Modify the 'osd-hosts' file as necessary
2. Modify and execute 'ceph-xfs-loop' to format the partitions with XFS
3. Modify and execute 'gen-uuid' (or run the command manually) to create a UUID file matching IPs to their respective block ID's (some modification post generation may be necessary)
4. Modify and execute 'ceph-osd-loop' to create OSDs, directories, keys, and automount entires

DO NOT USE the scripts located in node-exec-DONOTUSE without heavy modification.  They were from a previous environment and used for node-level execution of certain tasks when other components had been already completed.  Use for reference only.